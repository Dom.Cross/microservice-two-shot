from django.urls import path
from .views import (
    api_list_hats,
    api_list_locations,
    api_show_hats,
    api_show_locations,
)


urlpatterns = [
    path("hats/", api_list_hats, name="api_list_hats"),
    path(
        "shoes/<int:pk>/",
        api_show_hats,
        name="api_show_shoe",
    ),
    path("locations/", api_list_locations, name="api_list_locations"),
    path("locations/<int:pk>/", api_show_locations, name="api_show_locations"),
]
