import React, { useState, useEffect } from 'react';

function ShoeForm() {
  const [formData, setFormData] = useState({
    model_name: '',
    manufacturer: '',
    picture_url: '',
    color: '',
    bin: '',
  });

  const [bins, setBins] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [successMessage, setSuccessMessage] = useState('');
  const [errorMessage, setErrorMessage] = useState('');

  const handleSubmit = async (event) => {
    event.preventDefault();
    setIsLoading(true);

    const url = 'http://localhost:8080/api/shoes/';
    const fetchConfig = {
      method: 'post',
      body: JSON.stringify(formData),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    try {
      const response = await fetch(url, fetchConfig);
      if (response.ok) {
        setSuccessMessage('Shoe added successfully!');
        setFormData({
          model_name: '',
          manufacturer: '',
          picture_url: '',
          color: '',
          bin: '',
        });
      } else {
        setErrorMessage('Failed to add the shoe.');
      }
    } catch (e) {
      console.error('An error occurred during the fetch operation:', e);
      setErrorMessage('Error occurred while adding the shoe.');
    } finally {
      setIsLoading(false);
    }
  };

  const handleFormChange = (e) => {
    const value = e.target.value;
    const inputName = e.target.name;

    setFormData({
      ...formData,
      [inputName]: inputName === 'bin' ? parseInt(value) : value,
    });
  };

  useEffect(() => {
    async function fetchBins() {
      try {
        const url = 'http://localhost:8100/api/bins/';
        const response = await fetch(url);
        if (!response.ok) {
          throw new Error('Bad response while fetching bins!');
        }
        const data = await response.json();
        setBins(data.bins);
      } catch (e) {
        console.error('An error occurred during the fetch operation:', e);
        setErrorMessage('Error occurred while fetching bins.');
      }
    }

    fetchBins();
  }, []);

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Add a new shoe</h1>
          {errorMessage && <div className="alert alert-danger">{errorMessage}</div>}
          {successMessage && <div className="alert alert-success">{successMessage}</div>}
          <form onSubmit={handleSubmit} id="add-shoe-form">
            <div className="form-floating mb-3">
              <input onChange={handleFormChange} placeholder="Model Name" required type="text" name="model_name" id="model_name" className="form-control" value={formData.model_name} />
              <label htmlFor="model_name">Model Name</label>
            </div>

            <div className="form-floating mb-3">
              <input onChange={handleFormChange} placeholder="Manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control" value={formData.manufacturer} />
              <label htmlFor="manufacturer">Manufacturer</label>
            </div>

            <div className="form-floating mb-3">
              <input onChange={handleFormChange} placeholder="Picture URL" required type="text" name="picture_url" id="picture_url" className="form-control" value={formData.picture_url} />
              <label htmlFor="picture_url">Picture URL</label>
            </div>

            <div className="form-floating mb-3">
              <input onChange={handleFormChange} placeholder="Color" required type="text" name="color" id="color" className="form-control" value={formData.color} />
              <label htmlFor="color">Color</label>
            </div>

            <div className="mb-3">
              <label htmlFor="bin">Bin</label>
              <select onChange={handleFormChange} required name="bin" id="bin" className="form-select">
                <option value="">Choose a bin</option>
                {bins.map(bin => {
                  return (
                    <option key={bin.id} value={bin.id}>{bin.closet_name} - Bin {bin.bin_number}</option>
                  )
                })}
              </select>
            </div>
            <button className="btn btn-primary" disabled={isLoading}>
              {isLoading ? 'Adding...' : 'Add Shoe'}
            </button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default ShoeForm;