import React, { useState, useEffect } from 'react';
import ShoeItem from './ShoeItem';

function ShoePage() {
  const [shoesData, setShoesData] = useState([]);

  useEffect(() => {
    async function fetchData() {
      try {
        const url = 'http://localhost:8080/api/shoes/';
        const response = await fetch(url);
        if (!response.ok) {
          alert("Bad response!");
        } else {
          const data = await response.json();
          setShoesData(data.shoes);
        }
      } catch (e) {
        console.error("An error occurred during the fetch operation:", e);
        alert("Error was raised!");
      }
    }

    fetchData();
  }, []);

  const columnWidth = 300;

  const handleDelete = async (shoeId) => {

    try {
      const url = `http://localhost:8080/api/shoes/${shoeId}/`;
      const fetchConfig = {
        method: "DELETE",
      };
      const response = await fetch(url, fetchConfig);
      if (response.ok) {
        setShoesData((prevShoes) => prevShoes.filter((shoe) => shoe.id !== shoeId));
      } else {
        alert("Failed to delete the shoe!");
      }
    } catch (e) {
      console.error("An error occurred during the fetch operation:", e);
      alert("Error occurred!");
    }
  };

  return (
    <div style={{ display: "flex", flexWrap: "wrap" }}>
      {shoesData.map((shoe) => (
        <div key={shoe.id} style={{ width: columnWidth }}>
          <ShoeItem
            model_name={shoe.model_name}
            manufacturer={shoe.manufacturer}
            pictureUrl={shoe.picture_url}
            color={shoe.color}
            bin={shoe.bin.name}
            onDelete={() => handleDelete(shoe.id)}
          />
        </div>
      ))}
    </div>
  );
}

export default ShoePage;