import React from 'react';

function ShoeItem({ model_name, manufacturer, pictureUrl, color, bin, onDelete }) {
  return (
    <div className="shoe-item">
      <h2>{model_name}</h2>
      <p>Manufacturer: {manufacturer}</p>
      <img src={pictureUrl} alt={model_name} />
      <p>Color: {color}</p>
      <p>Bin: {bin}</p>
      <button onClick={onDelete}>Delete</button>
    </div>
  );
}

export default ShoeItem;
