# Generated by Django 4.0.3 on 2023-07-20 17:43

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shoes_rest', '0007_alter_shoe_options'),
    ]

    operations = [
        migrations.AddField(
            model_name='bin',
            name='import_href',
            field=models.CharField(default=False, max_length=100, unique=True),
        ),
    ]
