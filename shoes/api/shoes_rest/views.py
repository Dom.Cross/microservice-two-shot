from django.shortcuts import render
from django.http import JsonResponse, HttpResponseNotAllowed
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder
from .models import BinVO, Shoes
from django.shortcuts import get_object_or_404


class BinVOEncoder(ModelEncoder):
    model = BinVO
    properties = [
        "name",
        "import_href",
    ]


class ShoeEncoder(ModelEncoder):
    model = Shoes
    properties = [
        "model_name",
        "manufacturer",
        "color",
        "picture_url",
        "id",
        "bin"
    ]
    encoders = {
        "bin": BinVOEncoder(),
    }


class ShoeDetailEncoder(ModelEncoder):
    model = Shoes
    properties = [
        "model_name",
        "manufacturer",
        "color",
        "picture_url",
        "id",
        "bin",
    ]
    encoders = {
        "bin": BinVOEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_shoes(request):
    if request.method == "GET":
        shoes = Shoes.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoeEncoder,
        )
    else:
        content = json.loads(request.body)
        bin_id = content.pop("bin")
        bin_instance = BinVO.objects.get(pk=bin_id)
        shoe = Shoes.objects.create(bin=bin_instance, **content)
        return JsonResponse(
            shoe,
            encoder=ShoeEncoder,
            safe=False,
        )

@require_http_methods(["DELETE", "GET"])
def api_show_shoe(request, pk):
    if request.method =="GET":
        shoe = Shoes.objects.get(id=pk)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        shoe = get_object_or_404(Shoes, id=pk)
        shoe.delete()
        return JsonResponse({"message": "Shoe deleted successfully."})
    else:
        return HttpResponseNotAllowed(["DELETE", "GET"])
